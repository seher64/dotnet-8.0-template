# Dotnet 8.0.x template

This repository is a HelloWorld dotnet 8.0.x application template, with unit tests, dockerized and a setup pipeline which builds, runs and tests the application.

## How to use it

* Fork this repository
* To run the hello world application in docker run the following commands
  * `docker build -t dotnet_template .`
  * `docker run -it --rm dotnet_template`
* Or install dotnet 8.0 and build and run the project locally
* You can use this as a starting point for your own application
