# Use SDK image to build the project
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["dotnet_template/dotnet_template.csproj", "dotnet_template/"]
RUN dotnet restore "dotnet_template/dotnet_template.csproj"
COPY . .
WORKDIR "/src/dotnet_template"
RUN dotnet build "dotnet_template.csproj" -c Release -o /app/build

# Use runtime image for the final image
FROM mcr.microsoft.com/dotnet/runtime:8.0 AS final
WORKDIR /app
COPY --from=build /app/build .
ENTRYPOINT ["dotnet", "dotnet_template.dll"]
